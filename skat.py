class Game:
    def __init__(self, game=0, suit=0, by=0, hand=0, ouvert=0,
                 schneider=0, schwarz=0, announced=0):
        self.game = game
        self.suit = suit
        self.by = by
        self.hand = hand
        self.ouvert = ouvert
        self.schneider = schneider
        self.schwarz = schwarz
        self.announced = announced

    def value(self):
        return ((23 + self.game if self.game != 0 else self.suit)
                * (1 + self.by + self.hand + self.ouvert +
                   self.schneider + self.schwarz + self.announced))


class Face:
    def __init__(self, value):
        self.__value = value

    def __repr__(self):
        return self.longRep[self.__value]

    def __str__(self):
        return self.shortRep[self.__value]

    def __cmp__(self, other):
        return self.compare(other)

    def rank(self, game=Game()):
        return self._order(game).index(self.__value)

    def compare(self, other, game=Game()):
        if (self.rank(game) < other.rank(game)):
            return 1
        elif (self.rank(game) > other.rank(game)):
            return -1
        elif (self.rank(game) == other.rank(game)):
            return 0
        else:
            exit()


class Suit(Face):
    longRep = ("Diamonds", "Hearts", "Clubs", "Spade")
    shortRep = ("d", "h", "c", "s")
    _value = (9, 10, 11, 12)

    def _order(self, game=Game()):
        if (game.game == 1):
            return range(4)
        if (game.game == 2):
            return range(4)
        if (game.game == 0):
            val = game.suit
            ret = range(4)
            add = 0
            for i in range(3):
                if (i == val):
                    add = 1
                ret[i] = i + add
            ret[3] = val
            return ret


class Rank(Face):
    __suitOrder = (0, 1, 2, 5, 6, 3, 7, 4)
    longRep = ("7", "8", "9", "10", "Jack", "Queen", "King", "Ace")
    shortRep = ("7", "8", "9", "10", "j", "q", "k", "a")
    _value = (0, 0, 0, 10, 2, 3, 4, 11)

    def _order(self, game=Game()):
        if (game.game == 1):
            return range(8)
        if (game.game == 2):
            return range(4)
        if (game.game == 0):
            return self.__suitOrder


# Class that symbolizes a single Card.
class Card:
    # Constructor of a card.
    # Accepts a index number of suit (0,3) and rank (0,7)
    def __init__(self, suit, rank):
        self.__suit = Suit(suit)
        self.__rank = Rank(rank)

    # String short representation of a card
    def __str__(self):
        return str((self.__suit, self.__rank))

    # String long representation of a card
    def __repr__(self):
        return repr((self.__suit, self.__rank))

    # Comparator of two cards with grand and suit game order
    def compare(self, other):
        suitC = Suit.compare(self.__suit, other.__suit)
        rankC = Rank.compare(self.__rank, other.__rank)
        if (self.__rank.__repr__() == "Jack" and
                other.__rank.__repr__() == "Jack"):
            return suitC
        elif (self.__rank.__repr__() == "Jack"):
            return -1
        elif (other.__rank.__repr__() == "Jack"):
            return 1
        elif (suitC == 0):
            return rankC
        else:
            return suitC


games = []
for i in range(1, 11):
    for suit in range(4):
        s = Suit(suit)
        string = "With %i, game %i, played %s" % (i, i + 1, s.__repr__())
        games.append(((9 + suit) * (i + 1), string))
        string = "%s, schwartz %i" % (string, i + 2)
        games.append(((9 + suit) * (i + 2), string))


class Player:
    def calcGame(self):
        pass

    def __init__(self, name, cards, human=0):
        self.__name = name
        self.__cards = sorted(cards, cmp=Card.compare)
        self.__won = []
        self.__game = self.calcGame()
        self.__human = human

    def __str__(self):
        return "%s: %s" % (self.__name, self.__cards)

    def __repr__(self):
        return "%s: %s" % (self.__name, self.__cards)

    def bid(self, value):
        if(self.__game < value):
            return 1
        else:
            return 0

    def check(self, value):
        pass

    def playCard(self, value):
        pass

    def storeWon(self, cards):
        self.__won.append(cards)

    def calcPoints(self):
        points = 0
        for card in self.__won:
            points += card.value()
        return points

    def insertSkat(self, skat):
        pass

    def playHand(self, skat):
        pass


# Deals out the card deck onto the stacks, while the Skat stack is ignored
def dealOutCards(stacks, stacklist, deck):
    for stack in stacklist:
        if (stack == "Skat"):
            continue
        stacks[stack].append(deck.pop())


# Mixes a card deck and deal out the cards to all players with the skat
def deal():
    players = []
    deck = []
    for suit in range(4):
        for rank in range(8):
            deck.append(Card(suit, rank))
    stacks = {}
    stacklist = ("Player1", "Player2", "Player3", "Skat")
    for stack in stacklist:
        stacks[stack] = []
    import random
    random.shuffle(deck)
    for stack in stacklist:
        stacks[stack].append(deck.pop())
        stacks[stack].append(deck.pop())
    while len(deck) > 0:
        dealOutCards(stacks, stacklist, deck)
    for p in range(3):
        name = "Player%i" % (p + 1)
        players.append(Player(name, stacks[name]))
    return (players, stacks["Skat"])


players, skat = deal()
print players[0]

# Reizen

# geben, hören, sagen, weitersagen


# calc hand values


# Skat

# Spielen


# Zählen
